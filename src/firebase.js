import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/storage';


const config = {
  apiKey: "AIzaSyD-r6dzbW4q85Rijqh2vIg0fALEaYKvVBo",
  authDomain: "slack-clone-react-dev.firebaseapp.com",
  databaseURL: "https://slack-clone-react-dev.firebaseio.com",
  projectId: "slack-clone-react-dev",
  storageBucket: "slack-clone-react-dev.appspot.com",
  messagingSenderId: "225041754161"
};
firebase.initializeApp(config);

export default firebase;